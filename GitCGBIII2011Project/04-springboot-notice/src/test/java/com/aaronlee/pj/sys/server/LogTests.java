package com.aaronlee.pj.sys.server;

import org.junit.jupiter.api.Test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class LogTests {

    private static final  org.slf4j.Logger logger= LoggerFactory.getLogger(LogTests.class);

    /**
     * 测试警告级别
     * trace < debug < info < warn < error
     *  你如，设置为info时，显示info，warn，error
     */
    @Test
    void testLevel(){
        logger.trace("logger.lever.tarce");
        logger.debug("logger.lever.debug");
        logger.info("logger.lever.info");
        logger.warn("logger.lever.warn");
        logger.error("logger.lever.error");
        //logger.
    }
}
