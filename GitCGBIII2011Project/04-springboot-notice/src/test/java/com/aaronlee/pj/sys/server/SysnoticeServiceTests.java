package com.aaronlee.pj.sys.server;

import com.aaronlee.pj.sys.pojo.SysNotice;
import com.aaronlee.pj.sys.service.SysNoticeService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class SysnoticeServiceTests {
    @Autowired
    private SysNoticeService sysNoticeService;

    @Test
    void TestFindNotices(){
        SysNotice notice = new SysNotice();
        List<SysNotice> list = sysNoticeService.findNotices(notice);
        System.out.println(list);
    }

}
