package com.aaronlee.pj.sys.dao;


import com.aaronlee.pj.sys.pojo.SysNotice;
import com.mysql.cj.protocol.x.Notice;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.List;

@SpringBootTest
public class SysNoticeDaoTests {
    @Autowired  //加了此注解，系统底层会依赖注入对象
    @Resource
    private SysNoticeDao sysNoticeDao;

    @Test
    void testInsertNotice(){
        //创建SysNotice对象
        SysNotice notice = new SysNotice();
        notice.setTitle("CGB2011结课了，结课了？这辈子都不可能结课的");
        notice.setContent("2021/02/30正式结课");
        notice.setStatus("0");
        notice.setType("1");
        notice.setCreatedUser("tonny");
        notice.setModifiedUser("tonny");
        //
        sysNoticeDao.insertNotice(notice);
    }
    @Test
    void testupdateNotice(){
        //itle=#{title},type=#{type},content=#{content},#status=#{status},remark=#{remark},
        //        createdUser=#{createdUser},modifiedUser=#{modifiedUser}
        //创建SysNotice对象
        SysNotice notice = sysNoticeDao.selectById(1L);//先查询

        notice.setTitle("CGB2011结课了");
        notice.setType("2");
        notice.setContent("这辈子都不可能结课的");
        notice.setStatus("0");
        notice.setCreatedUser("王浩然");
        notice.setModifiedUser("王浩然");
        notice.setId(3L);
        sysNoticeDao.updateNotice(notice);
    }
    @Test
    void testSelectById(){
        SysNotice notice = sysNoticeDao.selectById(1L);
        System.out.println(notice);
    }
    /**
     * 删除操作
     */
    @Test
    void testDeleteById(){
        int rows = sysNoticeDao.deleteById();
        System.out.println("rows=" + rows);
    }

    /**
     * 按需求查询
     */
    @Test
    void testSelectNotices(){
        SysNotice notice = new SysNotice();
        //notice.setType("2");
        //notice.setTitle("CGB");
        //notice.setModifiedUser("王浩");
        List<SysNotice> sysNotices = sysNoticeDao.selectNotices(notice);
        for (SysNotice n :sysNotices) {
            System.out.println(n);
        }
    }

}
