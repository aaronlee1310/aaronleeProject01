package com.aaronlee.pj.sys.dao;

import org.apache.ibatis.session.SqlSession;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.Connection;

@SpringBootTest
public class MyBatisTests {
    /*
        SqlSession是mybatis
     */
    @Autowired
    private SqlSession sqlSession;//这里的sqlSession指向的是谁？（SqlSessionTemplate）
    @Test
    void testGetConnection(){
        //连接来自哪里？连接池--底层会自动将连接池注入给mybatis框架
        Connection connection = sqlSession.getConnection();
        System.out.println("connection" + connection);
    }
}
