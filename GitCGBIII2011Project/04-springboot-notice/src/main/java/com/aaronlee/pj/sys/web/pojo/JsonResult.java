package com.aaronlee.pj.sys.web.pojo;

public class JsonResult {
    /**
     * 状态码
     * 1表示ok 0表示error
     */
    private Integer state = 1;

    /**
     * 状态码信息
     */
    private String message = "ok";

    /**
     * 封装正确的查询结果
     */
    private Object data;
    /*
        当出现异常时，可以通过此构造方法对异常惊喜封装
     */
    //public JsonResult(Throwable exception){
    //    this(0,exception.getMessage());
    //}


    public JsonResult() {
    }

    public JsonResult(String message) {
        this.message = message;
    }

    public JsonResult(Integer state, String message) {
        this.state = state;
        this.message = message;
    }

    public JsonResult(Object data) {
        data = data;
    }
	//当出现异常时,可以通过此构造方法对异常信息进行封装
    public JsonResult(Throwable exception){//new JsonResult(exception);
        this(0,exception.getMessage());
    }
    public JsonResult(Integer state, String message, Object data) {
        this.state = state;
        this.message = message;
        this.data = data;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

}
