package com.aaronlee.pj.sys.service.aspect;

import com.aaronlee.pj.common.annotation.RequiredLog;
import com.aaronlee.pj.sys.pojo.SysLog;
import com.aaronlee.pj.sys.service.SysLogService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.lang.reflect.Method;
import java.sql.SQLOutput;
import java.util.Date;

import static jdk.nashorn.internal.runtime.regexp.joni.encoding.CharacterType.S;

/**
 * spring框架中由@Aspect注解描述的类型为切面类型，此切面类型中要有
 * 1）切入点的定义（用于约束哪些目标方法执行时，进行功能扩展）
 * 2）通知方法的定义（这样的方法中会封装要执行的扩展业务逻辑，例如日志记录）
 */
@Order(2)//数字越小优先级越高
@Slf4j
@Aspect
@Component
public class SysLogAspect {
    /**
     * @Pointcut注解用于定义切入点
     * @annotation(注解类全名) 为定义切入点的一种表达式，由表达式中的注解描述
     * 的方法为切入点方法，可以再目标业务执行时通过指定的通知方法进行功能增强！
     */
    @Pointcut("@annotation(com.aaronlee.pj.common.annotation.RequiredLog)")
    public void doLog(){}//此方法只负责承载切入点。没有实际意义，不需要实现

    /**
     * @Around 注解描述的方法用于执行业务拓展业务逻辑的方法。此方法可以通过连接点对象（joinPoint）调用目标方法
     * @param joinPoint
     * @return
     */
    @Around("doLog()")//@Around("@annotation(com.cy.pj.common.annotation.RequiredLog)")
    public Object doLogAround(ProceedingJoinPoint joinPoint) throws Throwable{
		System.out.println("SysLogAspect.@Around.before");
        long t1= System.currentTimeMillis();
        log.info("before {}",t1);
        try {
            //System.out.println("before:"+System.currentTimeMillis());
            Object result = joinPoint.proceed();//调用切入点方法
            //System.out.println("after:"+System.currentTimeMillis());
            long t2=System.currentTimeMillis();
            log.info("after {}",t2);
            //记录正常的行为日志
            doLogInfo(joinPoint,(t2-t1),null);
            return result;//此返回值会交给代理对象，代理对象传给调用方法
        }catch(Throwable e){
            e.printStackTrace();
            long t3=System.currentTimeMillis();
            log.error("exception {}",t3);
            //记录详细的异常行为日志
            doLogInfo(joinPoint,(t3-t1),e);
            throw e;
        }
    }

    //记录详细的用户操作日志

    private void doLogInfo(ProceedingJoinPoint joinPoint,long time,Throwable e) throws Exception {
        //1获取日志
        //1.1获取登录用户名  没有先给假数据
        String username = "tony";
        //1.2获取ip
        String ip = "127.0.0.1";
        //1.3获取操作(切入点方法上RequiredLog注解中operation属性值
        //获取目标的字节码对象
        //1..3.1获取目标对象所在的字节码对象
        Class<?> targetCls = joinPoint.getTarget().getClass();
        //1.3.2获取目标方法对象
        //1.3.2.1获取方法的签名属性
        Signature signature = joinPoint.getSignature();
        MethodSignature ms = (MethodSignature) signature;
        //1.3.2.2通过字节码对象以及方法信息获取目标方法对象
        Method targetMethod = targetCls.getDeclaredMethod(ms.getName(),ms.getParameterTypes());
        //1.3.3获取方法上的requiredLog注解
        RequiredLog requiredLog = targetMethod.getAnnotation(RequiredLog.class);
        //1.3.4获取注解中的operation属性
        String operation = requiredLog.operation();
        //1.4获取方法信息(切入点的方法信息（方法所在的类+方法名）
        String targetClsMethod = targetCls.getName()+"."+targetMethod.getName();
        //1.5获取方法参数
        //1.5.1获取实际参数值
        Object[] args = joinPoint.getArgs();
        //1.5.2将参数值转化为json字符串
        String jsonParamStr = new ObjectMapper().writeValueAsString(args);
        //2封装日志
        SysLog userLog = new SysLog();
        userLog.setUsername(username);
        userLog.setIp(ip);
        userLog.setOperation(operation);//获取切入点方法上的RequiredLog注解中operation属性值
        userLog.setMethod(targetClsMethod);//获取切入点的方法信息
        userLog.setParams(jsonParamStr);
        if(e!=null){
            userLog.setStatus(0);//操作状态
            userLog.setError(e.getMessage());//错误信息
        }
        userLog.setTime(time);
        userLog.setCreatedTime(new Date());

        //3记录日志
//        log.info("user.oper{}",userLog.toString());
        log.info("user.oper {}",new ObjectMapper().writeValueAsString(userLog));
//        SysLogService sysLogService = (SysLogService) joinPoint.proceed();
        sysLogService.saveLog(userLog);
    }
	@Autowired
	private SysLogService sysLogService;
}

