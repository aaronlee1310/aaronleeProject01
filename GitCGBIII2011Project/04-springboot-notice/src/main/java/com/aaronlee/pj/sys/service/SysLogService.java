package com.aaronlee.pj.sys.service;

import com.aaronlee.pj.sys.pojo.SysLog;

import java.util.List;

public interface SysLogService {
    void saveLog(SysLog sysLog);
    int deleteById(Long...ids);
    SysLog findById(Long id);
    List<SysLog> findLogs(SysLog sysLog);
}
