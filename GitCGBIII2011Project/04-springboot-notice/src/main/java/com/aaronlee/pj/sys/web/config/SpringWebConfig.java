package com.aaronlee.pj.sys.web.config;

import com.aaronlee.pj.sys.web.interceptor.TimeAccessInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 此注解为spring中的一个配置类bean对象
 */
@Configuration
public class SpringWebConfig implements WebMvcConfigurer {

    /**注册拦截器，并且设置要拦截的路径*/
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //注册拦截器（将拦截器添加到Spring容器中）
        registry.addInterceptor(new TimeAccessInterceptor())
                //设置要拦截的url
//                .addPathPatterns("/notice/**"/*,"/notice/doDeleteById/*"*/);//*表示通配符，**表示多层url
        .addPathPatterns("/notice/doFindNotices","/notice/doDeleteById/*");//可变参，还可以继续添加
    }

}
