package com.aaronlee.pj.sys.service.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * 通过此切面演示各种通知（before、after、AfterRunning、After……）的执行时间点
 */
@Order(1)
@Aspect
@Component
public class SysTimeAspect {
    /**切入点的定义*/
    @Pointcut("@annotation(com.aaronlee.pj.common.annotation.RequiredTime)")
    public void doTime(){}
    @Before("doTime()")
    public void doBefore(){
        System.out.println("@Before");
    }
    @After("doTime()")
    public void doAfter(){
        System.out.println("@After");
    }
    @AfterReturning("doTime()")
    public void doAfterReturning(){
        System.out.println("@AfterReturning");
    }
    @AfterThrowing("doTime()")
    public void doAfterThrowing(){
        System.out.println("@AfterThrowing");
    }
    @Around("doTime()")
    public Object doAround(ProceedingJoinPoint joinPoint) throws Throwable {
        System.out.println("SysTimeAspect.@Around.Before");
        try{
            Object result = joinPoint.proceed();//调用本类其他通知方法、后续其他
            System.out.println("@Around.AfterReturning");
            return result;

        }catch (Exception e){
            System.out.println("@Around.AfterThrowing");
            throw  e;
        }finally {
            System.out.println("@Around.After");
        }
    }
}
