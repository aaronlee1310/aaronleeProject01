package com.aaronlee.pj.sys.service.impl;

import com.aaronlee.pj.common.annotation.RequiredLog;
import com.aaronlee.pj.sys.dao.SysLogDao;
import com.aaronlee.pj.sys.pojo.SysLog;
import com.aaronlee.pj.sys.service.SysLogService;
import org.aspectj.lang.annotation.Around;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class SysLogServiceImpl implements SysLogService {
    @Autowired
    private SysLogDao sysLogDao;

    @Override
    public void saveLog(SysLog sysLog) {
        sysLogDao.insertLog(sysLog);
    }
	@RequiredLog(operation = "删除日志")

    @Override
    public int deleteById(Long... ids) {
        return sysLogDao.deleteById(ids);
    }

    @Override
    public SysLog findById(Long id) {
        return sysLogDao.selectById(id);
    }

    @Override
    public List<SysLog> findLogs(SysLog sysLog) {
        return sysLogDao.selectLogs(sysLog);
    }
}
