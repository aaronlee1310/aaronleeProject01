package com.aaronlee.pj.sys.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/*
    SysNotice对象用于存储数据

    java中的对象可以分为两大类型：
        一类负责执行逻辑（打天下-控制逻辑，业务逻辑，数据持久逻辑）的对象
        一类负责存储数据（守天下-pojo/domain）的对象
            一个对象靠什么存储数据？属性
 */
//@Setter
//@Getter
//@ToString
@Data //此注解，类中会自动添加setter getter toString hashCode equals等方法
@NoArgsConstructor //为类添加无参构造函数
@AllArgsConstructor //未类添加全参构造函数
public class SysNotice {
    //private static final long serialVersionUID = 1L;
    /** 公告 ID */
    private Long id;
    /** 公告标题 */
    private String title;
    /** 公告类型（1 通知 2 公告） */
    private String type;
    /** 公告内容 */
    private String content;
    /** 公告状态（0 正常 1 关闭） */
    private String status;
    /** 创建时间 */
	//当使用DateTimeFormat注解描述此属性时,表示客户端需要按此格式为属性传入值
    @DateTimeFormat(pattern = "yyyy/MM/dd HH:mm:ss")//默认
    //当使用JsonFormat注解描述方法参数时,表示调用此属性的get方法取值并转换为字符串
    //时,按照pattern指定的格式进行转换
    @JsonFormat(pattern = "yyyy/MM/dd HH:mm:ss",timezone = "GMT+8")
    private java.util.Date createdTime;
    /** 修改时间*/
    private java.util.Date modifiedTime;
    /** 创建用户 */
    private String createdUser;
    /** 修改用户*/
    private String modifiedUser;
    /**备注*/
    private  String remark;

    public void setRemark(String remark) {
        this.remark = remark;
    }
//
//    public void setRemark(String remark) {
//        this.remark = remark;
//    }
//
//    public SysNotice() {
//    }
//    //自己添加 set/get/toString 方法
//
//    public static long getSerialVersionUID() {
//        return serialVersionUID;
//    }
//
//    public Long getId() {
//        return id;
//    }
//
//    public void setId(Long id) {
//        this.id = id;
//    }
//
//    public String getTitle() {
//        return title;
//    }
//
//    public void setTitle(String title) {
//        this.title = title;
//    }
//
//    public String getType() {
//        return type;
//    }
//
//    public void setType(String type) {
//        this.type = type;
//    }
//
//    public String getContent() {
//        return content;
//    }
//
//    public void setContent(String content) {
//        this.content = content;
//    }
//
//    public String getStatus() {
//        return status;
//    }
//
//    public void setStatus(String status) {
//        this.status = status;
//    }
//
//    public Date getCreatedTime() {
//        return createdTime;
//    }
//
//    public void setCreatedTime(Date createdTime) {
//        this.createdTime = createdTime;
//    }
//
//    public Date getModifiedTime() {
//        return modifiedTime;
//    }
//
//    public void setModifiedTime(Date modifiedTime) {
//        this.modifiedTime = modifiedTime;
//    }
//
//    public String getCreatedUser() {
//        return createdUser;
//    }
//
//    public void setCreatedUser(String createdUser) {
//        this.createdUser = createdUser;
//    }
//
//    public String getModifiedUser() {
//        return modifiedUser;
//    }
//
//    public void setModifiedUser(String modifiedUser) {
//        this.modifiedUser = modifiedUser;
//    }
//
//    @Override
//    public String toString() {
//        return "SysNotice{" +
//                "id=" + id +
//                ", title='" + title + '\'' +
//                ", type='" + type + '\'' +
//                ", content='" + content + '\'' +
//                ", status='" + status + '\'' +
//                ", createdTime=" + createdTime +
//                ", modifiedTime=" + modifiedTime +
//                ", createdUser='" + createdUser + '\'' +
//                ", modifiedUser='" + modifiedUser + '\'' +
//                '}';
//    }
}
