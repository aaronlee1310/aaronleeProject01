package com.aaronlee.pj.sys.web.controller;

import com.aaronlee.pj.sys.pojo.SysNotice;
import com.aaronlee.pj.sys.service.SysNoticeService;
import com.aaronlee.pj.sys.web.pojo.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

//import java.util.List;


/**
 * 这个类我们称之为控制层对象的处理器(Handler),通过此对象处理
 * DispatcherServlet(核心控制器-Controller)分发的请求,具体的
 * 处理包括:
 * 1)定义请求url映射
 * 2)通过参数对象封装请求参数数据
 * 3)调用业务方法处理业务逻辑
 * 4)封装处理结果交给DispatcherServlet进行处理.
 */
@RestController  //@Controller+@ResponseBody
public class SysNoticeController {//这里写的controller又称之为handler
    @Autowired
    private SysNoticeService sysNoticeService;
    //http://localhost:1314/notice/doFindNotices
    //http://localhost:1314/notice/doFindNotices?type=1&modifiedUser=tony
    //此方法会由DispatcherServlet (controller)对象通过反射进行调用
    //dispatcherServlet拿到请求中参数时会将参数注入给反射调用的方法参数
    //@RequestMapping(value="/notice/doFindNotices",method = RequestMethod.GET)
    @GetMapping("/notice/doFindNotices")
    public JsonResult doFindNotices(SysNotice notice){
        return new JsonResult(sysNoticeService.findNotices(notice));
    }
    /**
     * @RequestMapping(value="/notice/doSaveNotice",method = RequestMethod.POST)
     * 假如方法参数使用了@RequestBody描述,客户端数据的提交要求:
     * 1)请求方式:post
     * 2)请求Content-Type设计:application/json
     * 3)请求数据格式{"key1":"v1","key2":2,....}
     * @param notice
     * @return
     */
    @PostMapping("/notice/doSaveNotice")
    public JsonResult doSaveNotice(@RequestBody SysNotice notice){
//        try {
        sysNoticeService.saveNotice(notice);
//        }catch(Throwable e){
//            return new JsonResult(e);
//        }
        return new JsonResult("save ok");
    }
    /**基于id查询notice对象*/
    //rest风格url(软件架构编码风格),允许在url中基于{}方式定义变量
    //rest风格应用目的:让url的设计更加通用和简单
    //访问url: http://ip:port/notice/doFindById/14
    //方法参数的值假如来自url中{var}变量的值,则需要使用@PathVariable注解对参数进行描述
    @GetMapping("/notice/doFindById/{id}")
    public JsonResult doFindById(@PathVariable Long id){
        return  new JsonResult(sysNoticeService.findById(id));
    }

    @PutMapping("/notice/doUpdateNotice")
    public JsonResult doUpdateNotice(@RequestBody SysNotice notice){
        sysNoticeService.updateNotice(notice);
        return new JsonResult("update ok");
    }

    @DeleteMapping("/notice/doDeleteById/{ids}")
    public JsonResult doDeleteById(@PathVariable  Long... ids){
        sysNoticeService.deleteById(ids);
        return new JsonResult("delete ok");
    }
}
/**
 * 请求响应处理
 * 请求数据:
 * 1)请求url的定义(普通风格url,rest风格url)
 * 2)请求方式的设计 (@GetMapping,@PostMapping,@DeleteMapping,@PutMapping,@RequestMapping)
 * 3)请求方法参数类型设计(直接量,pojo对象)
 * 4)请求方法参数修饰(@PathVariable->从url中取数据,@RequestBody-告诉服务端接收json数据,@RequestParam-对传统请求参数进行约束)
 * 响应数据
 * 1)响应标准设计
 * 2)响应数据转换(json格式数据)
 * 3)统一异常的处理(?)
 */
