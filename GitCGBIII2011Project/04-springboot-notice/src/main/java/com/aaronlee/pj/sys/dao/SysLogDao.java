package com.aaronlee.pj.sys.dao;

import com.aaronlee.pj.sys.pojo.SysLog;
import com.aaronlee.pj.sys.pojo.SysNotice;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface SysLogDao {

    int insertLog(SysLog entity);

    int deleteById(Long... ids);

//    @Select("selete * from sys_logs where id = #{id}")
    SysLog selectById(Long id);

    List<SysLog> selectLogs(SysLog sysLog);
}
