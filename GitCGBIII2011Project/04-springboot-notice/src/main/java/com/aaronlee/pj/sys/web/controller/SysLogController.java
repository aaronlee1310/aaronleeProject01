package com.aaronlee.pj.sys.web.controller;

import com.aaronlee.pj.sys.pojo.SysLog;
import com.aaronlee.pj.sys.service.SysLogService;
import com.aaronlee.pj.sys.web.pojo.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/log/")
@RestController
public class SysLogController {
    @Autowired
    private SysLogService sysLogService;

    @GetMapping("doFindLogs")
    public JsonResult doFindLogs(SysLog sysLog){
       return new JsonResult(sysLogService.findLogs(sysLog));
    }

}
